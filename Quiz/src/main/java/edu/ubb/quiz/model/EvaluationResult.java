package edu.ubb.quiz.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="quiz_result")
public class EvaluationResult {

private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer resultId;
	
	@Lob
	private String result;
	private int minPoints;
	private int maxPoints;
	private String imagePath;
	
	@NotNull
	@ManyToOne(optional=false, fetch = FetchType.EAGER)
	@JoinColumn(name = "quizId")
	private Quiz quiz;
	
	public EvaluationResult() {}

	public EvaluationResult(Integer resultId, String result, int minPoints, int maxPoints, String imagePath, Quiz quiz) {
		super();
		this.resultId = resultId;
		this.result = result;
		this.minPoints = minPoints;
		this.maxPoints = maxPoints;
		this.imagePath = imagePath;
		this.quiz = quiz;
	}

	public Integer getResultId() {
		return resultId;
	}

	public void setResultId(Integer resultId) {
		this.resultId = resultId;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getMinPoints() {
		return minPoints;
	}

	public void setMinPoints(int minPoints) {
		this.minPoints = minPoints;
	}

	public int getMaxPoints() {
		return maxPoints;
	}

	public void setMaxPoints(int maxPoints) {
		this.maxPoints = maxPoints;
	}
	
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Quiz getQuiz() {
		return quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}
	
}
