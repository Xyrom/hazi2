package edu.ubb.quiz.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.ubb.quiz.model.enums.QuizType;

@Entity
@Table(name="quiz_type")
public class Quiz {

private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer quizId;
	
	private String name;
	private String description;
	
	@Enumerated(EnumType.STRING)
	private QuizType type;
	
	@OneToMany(mappedBy="quiz", cascade=CascadeType.ALL)
	private Set<Question> questions = new HashSet<Question>();
	
	@OneToMany(mappedBy="quiz", cascade=CascadeType.ALL)
	private Set<EvaluationResult> results = new HashSet<EvaluationResult>();
	
	private String imagePath;
	private Integer questionNr;
	
	public Quiz() {}
	
	public Integer getQuizId() {
		return quizId;
	}

	public void setQuizId(Integer quizId) {
		this.quizId = quizId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonIgnore
	public Set<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}

	@JsonIgnore
	public Set<EvaluationResult> getResults() {
		return results;
	}

	public void setResults(Set<EvaluationResult> results) {
		this.results = results;
	}

	public QuizType getType() {
		return type;
	}

	public void setType(QuizType type) {
		this.type = type;
	}
	
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Integer getQuestionNr() {
		return questionNr;
	}

	public void setQuestionNr(Integer questionNr) {
		this.questionNr = questionNr;
	}
	
}
