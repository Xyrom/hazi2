package edu.ubb.quiz.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="quiz_answer")
public class Answer {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer answerId;

	private String answer;
	private Integer answersPoint;
	
	@NotNull
	@ManyToOne(optional=false, fetch = FetchType.EAGER)
	@JoinColumn(name = "questionId")
	private Question question;
	
	public Answer() {}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getAnswersPoint() {
		return answersPoint;
	}

	public void setAnswersPoint(Integer answersPoint) {
		this.answersPoint = answersPoint;
	}

	@JsonIgnore
	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}
	
}
