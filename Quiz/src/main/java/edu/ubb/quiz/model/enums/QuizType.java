package edu.ubb.quiz.model.enums;

public enum QuizType {
	PICK_ONE, FILL_IN, PICK_ONE_CORRECT
}
