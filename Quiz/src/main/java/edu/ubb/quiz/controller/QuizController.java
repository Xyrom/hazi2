package edu.ubb.quiz.controller;

import java.util.List;

import edu.ubb.quiz.model.enums.QuizType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.ubb.quiz.model.Quiz;
import edu.ubb.quiz.service.QuizService;

@RestController
@RequestMapping("/api/quiz")
public class QuizController {
	
	@Autowired
	private QuizService quizTypeService;
	
	@GetMapping("/quizzes")
	public List<Quiz> getAllTypes() {
		return quizTypeService.getAllTypes();
	}

	@GetMapping("/quizzes/{quizType}")
	public List<Quiz> getQuizzesByType(@PathVariable QuizType quizType) {
		return quizTypeService.getQuizzesByType(quizType);
	}
	
}