package edu.ubb.quiz.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.ubb.quiz.model.Answer;
import edu.ubb.quiz.model.EvaluationResult;
import edu.ubb.quiz.model.enums.QuizType;
import edu.ubb.quiz.service.EvaluationResultService;

@RestController
@RequestMapping("/api/result")
public class EvaluationResultController {

	@Autowired
	private EvaluationResultService evaluationResultService;
	
	@PostMapping("/evaluate/{quizId}/{quizType}")
	public EvaluationResult getResult(@PathVariable Integer quizId, @PathVariable QuizType quizType, @RequestBody List<Answer> answers) {
		return evaluationResultService.getResult(quizId, quizType, answers);
	}
	
}
