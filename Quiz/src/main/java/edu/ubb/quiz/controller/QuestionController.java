package edu.ubb.quiz.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.ubb.quiz.model.Question;
import edu.ubb.quiz.model.enums.QuizType;
import edu.ubb.quiz.service.QuestionService;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {

	@Autowired
	private QuestionService questionService;
	
	@GetMapping("/{quizId}/{quizType}")
	public List<Question> createQuiz(@PathVariable Integer quizId, @PathVariable QuizType quizType) {
		return questionService.createQuiz(quizId, quizType);
	}
	//http://www.thayerbirding.com/v77-Features/Quizzes//
}
