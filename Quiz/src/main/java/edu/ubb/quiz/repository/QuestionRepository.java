package edu.ubb.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.ubb.quiz.model.Question;

public interface QuestionRepository extends JpaRepository<Question, Integer> {

	List<Question> findAllByQuizQuizId(Integer quizId);
	
}
