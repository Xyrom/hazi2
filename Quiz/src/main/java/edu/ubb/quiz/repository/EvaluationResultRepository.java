package edu.ubb.quiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.ubb.quiz.model.EvaluationResult;

public interface EvaluationResultRepository extends JpaRepository<EvaluationResult, Integer> {

	@Query("SELECT r FROM EvaluationResult r WHERE (:point >= r.minPoints AND :point <= r.maxPoints) AND r.quiz.quizId = :quizId")
	EvaluationResult findResult(@Param("quizId") Integer quizId, @Param("point") Integer point);
	
	@Query("SELECT q.questionNr FROM Quiz q, EvaluationResult r WHERE r.quiz.quizId = :quizId AND r.quiz.quizId = q.quizId")
	Integer findQuestionNr(@Param("quizId") Integer quizId);
}
