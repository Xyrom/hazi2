package edu.ubb.quiz.repository;

import edu.ubb.quiz.model.enums.QuizType;
import org.springframework.data.jpa.repository.JpaRepository;

import edu.ubb.quiz.model.Quiz;

import java.util.List;

public interface QuizRepository extends JpaRepository<Quiz, Integer> {

    List<Quiz> findAllByType(QuizType quizType);
}
