package edu.ubb.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ubb.quiz.model.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Integer> {
	List<Answer> findAllByQuestionQuizQuizId(Integer quizId);
}
