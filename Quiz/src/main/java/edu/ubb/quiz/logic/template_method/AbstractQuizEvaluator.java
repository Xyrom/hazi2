package edu.ubb.quiz.logic.template_method;

import java.util.List;

import edu.ubb.quiz.model.Answer;
import edu.ubb.quiz.model.EvaluationResult;
import edu.ubb.quiz.repository.AnswerRepository;
import edu.ubb.quiz.repository.EvaluationResultRepository;

public abstract class AbstractQuizEvaluator {

	private Integer quizId;

	public EvaluationResult evaluateQuizByAnswers(Integer quizId, List<Answer> givenAnswers, EvaluationResultRepository evaluationResultRepository, AnswerRepository answerRepository) {
		this.quizId = quizId;

		this.getAnswers(answerRepository);
		return this.getResult(this.evaluate(givenAnswers), evaluationResultRepository);
	}

	public abstract void getAnswers(AnswerRepository answerRepository);

	public abstract Integer evaluate(List<Answer> givenAnswers);

	public abstract EvaluationResult getResult(Integer point, EvaluationResultRepository evaluationResultRepository);

	public Integer getQuizId() {
		return quizId;
	}

	public void setQuizId(Integer quizId) {
		this.quizId = quizId;
	}

}
