package edu.ubb.quiz.logic.factory;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import edu.ubb.quiz.model.Question;
import edu.ubb.quiz.repository.QuestionRepository;

public class FillInQuizFactory implements IQuizFactory {

	public List<Question> getQuestions(Integer qiuzId, QuestionRepository questionRepository) {
		return removeTheAnswers(sortTheQuestions(questionRepository.findAllByQuizQuizId(qiuzId)));
	}

	public List<Question> shuffleTheQuestions(List<Question> questions) {
		return null;
	}

	public List<Question> sortTheQuestions(List<Question> questions) {
		Collections.sort(questions, (a, b) -> {
			return a.getQuestionId().compareTo(b.getQuestionId());
		});
		return questions;
	}

	public List<Question> removeTheAnswers(List<Question> questions) {
		ListIterator questionsIterator = questions.listIterator();

		while (questionsIterator.hasNext()) {
			Question question = (Question) questionsIterator.next();
			question.setAnswers(null);
			questionsIterator.set(question);
		}

		return questions;
	}

}
