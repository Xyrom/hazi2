package edu.ubb.quiz.logic.template_method;

import java.util.Iterator;
import java.util.List;

import edu.ubb.quiz.model.Answer;
import edu.ubb.quiz.model.EvaluationResult;
import edu.ubb.quiz.repository.AnswerRepository;
import edu.ubb.quiz.repository.EvaluationResultRepository;

public class FillInQuizEvaluator extends AbstractQuizEvaluator {
	List<Answer> correctAnswers;
	
	@Override
	public void getAnswers(AnswerRepository answerRepository) {
		this.correctAnswers = answerRepository.findAllByQuestionQuizQuizId(this.getQuizId());
	}
	
	@Override
	public Integer evaluate(List<Answer> givenAnswers) {
		Iterator<Answer> itrCorrect = this.correctAnswers.iterator();
		Iterator<Answer> itrGiven = givenAnswers.iterator();
		int correctNr = 0;
		
		while(itrCorrect.hasNext() && itrGiven.hasNext()) {
			Answer correct = itrCorrect.next();
			Answer given = itrGiven.next();
			if(correct.getAnswer().equals(given.getAnswer())) {
				correctNr++;
			}
		}
		return correctNr;

	}

	@Override
	public EvaluationResult getResult(Integer point, EvaluationResultRepository evaluationResultRepository) {
		EvaluationResult e = new EvaluationResult();
		e.setResult(point + "/" + evaluationResultRepository.findQuestionNr(this.getQuizId()));
		e.setImagePath("\\assets\\fill_in.jpg");
		return e;
	}

}
