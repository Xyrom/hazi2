package edu.ubb.quiz.logic.template_method;

import java.util.List;

import edu.ubb.quiz.model.Answer;
import edu.ubb.quiz.model.EvaluationResult;
import edu.ubb.quiz.repository.AnswerRepository;
import edu.ubb.quiz.repository.EvaluationResultRepository;

public class PickOneQuizEvaluator extends AbstractQuizEvaluator {

	@Override
	public void getAnswers(AnswerRepository answerRepository) {		
	}

	@Override
	public Integer evaluate(List<Answer> givenAnswers) {
		Integer point = 0;
		
		for(int i = 0; i < givenAnswers.size(); i++) {
			point += givenAnswers.get(i).getAnswersPoint();
		}
		return point;
	}

	@Override
	public EvaluationResult getResult(Integer point, EvaluationResultRepository evaluationResultRepository) {
		return evaluationResultRepository.findResult(this.getQuizId(), point);
	}

}
