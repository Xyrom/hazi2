package edu.ubb.quiz.logic.factory;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import edu.ubb.quiz.model.Question;
import edu.ubb.quiz.repository.QuestionRepository;

public class PickOneQuizFactory implements IQuizFactory {
	
	public List<Question> getQuestions(Integer qiuzId, QuestionRepository questionRepository) {
		return shuffleTheQuestions(questionRepository.findAllByQuizQuizId(qiuzId));
	}

	public List<Question> shuffleTheQuestions(List<Question> questions) {
		Collections.shuffle(questions);
		return questions;
	}

}
