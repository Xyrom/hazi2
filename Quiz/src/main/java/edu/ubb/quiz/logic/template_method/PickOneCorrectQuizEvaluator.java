package edu.ubb.quiz.logic.template_method;

import edu.ubb.quiz.model.EvaluationResult;
import edu.ubb.quiz.repository.EvaluationResultRepository;

public class PickOneCorrectQuizEvaluator extends PickOneQuizEvaluator {
	
	@Override
	public EvaluationResult getResult(Integer point, EvaluationResultRepository evaluationResultRepository) {
		EvaluationResult evaluationResult = evaluationResultRepository.findResult(this.getQuizId(), point);
		evaluationResult.setResult(point + "/" + evaluationResultRepository.findQuestionNr(this.getQuizId()) + " \n " + evaluationResult.getResult());
		return evaluationResult;
	}
}
