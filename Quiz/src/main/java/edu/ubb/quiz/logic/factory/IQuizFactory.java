package edu.ubb.quiz.logic.factory;

import java.util.List;

import edu.ubb.quiz.model.Question;
import edu.ubb.quiz.repository.QuestionRepository;

public interface IQuizFactory {
//	public List<Question> makeQuiz();
	public List<Question> getQuestions(Integer qiuzId, QuestionRepository questionRepository);
	public List<Question> shuffleTheQuestions(List<Question> questions);
}
