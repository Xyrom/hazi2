package edu.ubb.quiz.service;

import java.util.List;

import edu.ubb.quiz.model.enums.QuizType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ubb.quiz.model.Quiz;
import edu.ubb.quiz.repository.QuizRepository;

@Service
public class QuizService {

	@Autowired
	private QuizRepository quizTypeRepository;
	
	public List<Quiz> getAllTypes() {
		return quizTypeRepository.findAll();
	}

    public List<Quiz> getQuizzesByType(QuizType quizType) {
		return quizTypeRepository.findAllByType(quizType);
    }
}
