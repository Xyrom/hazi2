package edu.ubb.quiz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ubb.quiz.logic.factory.FillInQuizFactory;
import edu.ubb.quiz.logic.factory.IQuizFactory;
import edu.ubb.quiz.logic.factory.PickOneQuizFactory;
import edu.ubb.quiz.model.Question;
import edu.ubb.quiz.model.enums.QuizType;
import edu.ubb.quiz.repository.QuestionRepository;

@Service
public class QuestionService {

	@Autowired
	private QuestionRepository questionRepository;
	private IQuizFactory quizFactory;

	public List<Question> createQuiz(Integer quizId, QuizType quizType) {
		if ((quizType == QuizType.PICK_ONE) || (quizType == QuizType.PICK_ONE_CORRECT)) {
			quizFactory = new PickOneQuizFactory();
		} else if(quizType == QuizType.FILL_IN) {
			quizFactory = new FillInQuizFactory();
		}
		return quizFactory.getQuestions(quizId, questionRepository);
	}

}
