package edu.ubb.quiz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ubb.quiz.logic.factory.PickOneQuizFactory;
import edu.ubb.quiz.logic.template_method.AbstractQuizEvaluator;
import edu.ubb.quiz.logic.template_method.FillInQuizEvaluator;
import edu.ubb.quiz.logic.template_method.PickOneCorrectQuizEvaluator;
import edu.ubb.quiz.logic.template_method.PickOneQuizEvaluator;
import edu.ubb.quiz.model.Answer;
import edu.ubb.quiz.model.EvaluationResult;
import edu.ubb.quiz.model.enums.QuizType;
import edu.ubb.quiz.repository.AnswerRepository;
import edu.ubb.quiz.repository.EvaluationResultRepository;

@Service
public class EvaluationResultService {

	@Autowired
	private EvaluationResultRepository evaluationResultRepository;
	@Autowired
	private AnswerRepository answerRepository;

	private AbstractQuizEvaluator quizEvaluator;

	public EvaluationResult getResult(Integer quizId, QuizType quizType, List<Answer> answers) {
		if (quizType == QuizType.PICK_ONE) {
			quizEvaluator = new PickOneQuizEvaluator();
		} else if (quizType == QuizType.PICK_ONE_CORRECT) {
			quizEvaluator = new PickOneCorrectQuizEvaluator();
		} else if (quizType == QuizType.FILL_IN) {
			quizEvaluator = new FillInQuizEvaluator();
		}
		return quizEvaluator.evaluateQuizByAnswers(quizId, answers, evaluationResultRepository, answerRepository);
	}

}
