webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_questions_questions_component__ = __webpack_require__("../../../../../src/app/components/questions/questions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_quizzes_quizzes_component__ = __webpack_require__("../../../../../src/app/components/quizzes/quizzes.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_quizzes_quiz_service__ = __webpack_require__("../../../../../src/app/services/quizzes/quiz.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_questions_questions_service__ = __webpack_require__("../../../../../src/app/services/questions/questions.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_fill_in_questions_fill_in_questions_component__ = __webpack_require__("../../../../../src/app/components/fill-in-questions/fill-in-questions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var appRoutes = [
    { path: 'quiz/:name', component: __WEBPACK_IMPORTED_MODULE_6__components_questions_questions_component__["a" /* QuestionsComponent */] },
    { path: '', component: __WEBPACK_IMPORTED_MODULE_7__components_quizzes_quizzes_component__["a" /* QuizzesComponent */], pathMatch: 'full' }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_quizzes_quizzes_component__["a" /* QuizzesComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_questions_questions_component__["a" /* QuestionsComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_fill_in_questions_fill_in_questions_component__["a" /* FillInQuestionsComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot(appRoutes, { useHash: true }),
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_9__services_questions_questions_service__["a" /* QuestionsService */], __WEBPACK_IMPORTED_MODULE_8__services_quizzes_quiz_service__["a" /* QuizService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/fill-in-questions/fill-in-questions.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#qid {\n    padding: 10px 15px;\n    border-radius: 20px;\n  }\n  label.btn {\n      padding: 18px 60px;\n      white-space: normal;\n      -webkit-transform: scale(1.0);\n      -moz-transform: scale(1.0);\n      -o-transform: scale(1.0);\n      -webkit-transition-duration: .3s;\n      -moz-transition-duration: .3s;\n      -o-transition-duration: .3s\n  }\n  \n  label.btn:hover {\n      text-shadow: 0 3px 2px rgba(0,0,0,0.4);\n      -webkit-transform: scale(1.1);\n      -moz-transform: scale(1.1);\n      -o-transform: scale(1.1)\n  }\n  label.btn-block {\n      text-align: left;\n      position: relative;\n      margin-top: 15px;\n  }\n  \n  label .btn-label {\n      position: absolute;\n      left: 0;\n      top: 0;\n      display: inline-block;\n      padding: 0 10px;\n      background: rgba(0,0,0,.15);\n      height: 100%\n  }\n  \n  label .glyphicon {\n      top: 34%\n  }\n  .element-animation1 {\n      animation: animationFrames ease .8s;\n      animation-iteration-count: 1;\n      transform-origin: 50% 50%;\n      -webkit-animation: animationFrames ease .8s;\n      -webkit-animation-iteration-count: 1;\n      -webkit-transform-origin: 50% 50%;\n      -ms-animation: animationFrames ease .8s;\n      -ms-animation-iteration-count: 1;\n      -ms-transform-origin: 50% 50%\n  }\n  .element-animation2 {\n      animation: animationFrames ease 1s;\n      animation-iteration-count: 1;\n      transform-origin: 50% 50%;\n      -webkit-animation: animationFrames ease 1s;\n      -webkit-animation-iteration-count: 1;\n      -webkit-transform-origin: 50% 50%;\n      -ms-animation: animationFrames ease 1s;\n      -ms-animation-iteration-count: 1;\n      -ms-transform-origin: 50% 50%\n  }\n  .element-animation3 {\n      animation: animationFrames ease 1.2s;\n      animation-iteration-count: 1;\n      transform-origin: 50% 50%;\n      -webkit-animation: animationFrames ease 1.2s;\n      -webkit-animation-iteration-count: 1;\n      -webkit-transform-origin: 50% 50%;\n      -ms-animation: animationFrames ease 1.2s;\n      -ms-animation-iteration-count: 1;\n      -ms-transform-origin: 50% 50%\n  }\n  .element-animation4 {\n      animation: animationFrames ease 1.4s;\n      animation-iteration-count: 1;\n      transform-origin: 50% 50%;\n      -webkit-animation: animationFrames ease 1.4s;\n      -webkit-animation-iteration-count: 1;\n      -webkit-transform-origin: 50% 50%;\n      -ms-animation: animationFrames ease 1.4s;\n      -ms-animation-iteration-count: 1;\n      -ms-transform-origin: 50% 50%\n  }\n  @keyframes animationFrames {\n      0% {\n          opacity: 0;\n          transform: translate(-1500px,0px)\n      }\n  \n      60% {\n          opacity: 1;\n          transform: translate(30px,0px)\n      }\n  \n      80% {\n          transform: translate(-10px,0px)\n      }\n  \n      100% {\n          opacity: 1;\n          transform: translate(0px,0px)\n      }\n  }\n\n  .modal-dialog {\n      margin-top: 5%;\n  }\n  \n  .modal-header {\n      background-color: transparent;\n      color: inherit\n  }\n  \n  .modal-body {\n      min-height: 355px\n  }\n  #loadbar {\n      position: absolute;\n      width: 62px;\n      height: 77px;\n      top: 2em\n  }\n  .blockG {\n      position: absolute;\n      background-color: #FFF;\n      width: 10px;\n      height: 24px;\n      -moz-border-radius: 8px 8px 0 0;\n      -moz-transform: scale(0.4);\n      -moz-animation-name: fadeG;\n      -moz-animation-duration: .8800000000000001s;\n      -moz-animation-iteration-count: infinite;\n      -moz-animation-direction: linear;\n      -webkit-border-radius: 8px 8px 0 0;\n      -webkit-transform: scale(0.4);\n      -webkit-animation-name: fadeG;\n      -webkit-animation-duration: .8800000000000001s;\n      -webkit-animation-iteration-count: infinite;\n      -webkit-animation-direction: linear;\n      -ms-border-radius: 8px 8px 0 0;\n      -ms-transform: scale(0.4);\n      -ms-animation-name: fadeG;\n      -ms-animation-duration: .8800000000000001s;\n      -ms-animation-iteration-count: infinite;\n      -ms-animation-direction: linear;\n      -o-border-radius: 8px 8px 0 0;\n      -o-transform: scale(0.4);\n      -o-animation-name: fadeG;\n      -o-animation-duration: .8800000000000001s;\n      -o-animation-iteration-count: infinite;\n      -o-animation-direction: linear;\n      border-radius: 8px 8px 0 0;\n      transform: scale(0.4);\n      animation-name: fadeG;\n      animation-duration: .8800000000000001s;\n      animation-iteration-count: infinite;\n      animation-direction: linear\n  }\n  #rotateG_01 {\n      left: 0;\n      top: 28px;\n      -moz-animation-delay: .33s;\n      -moz-transform: rotate(-90deg);\n      -webkit-animation-delay: .33s;\n      -webkit-transform: rotate(-90deg);\n      -ms-animation-delay: .33s;\n      -ms-transform: rotate(-90deg);\n      -o-animation-delay: .33s;\n      -o-transform: rotate(-90deg);\n      animation-delay: .33s;\n      transform: rotate(-90deg)\n  }\n  #rotateG_02 {\n      left: 8px;\n      top: 10px;\n      -moz-animation-delay: .44000000000000006s;\n      -moz-transform: rotate(-45deg);\n      -webkit-animation-delay: .44000000000000006s;\n      -webkit-transform: rotate(-45deg);\n      -ms-animation-delay: .44000000000000006s;\n      -ms-transform: rotate(-45deg);\n      -o-animation-delay: .44000000000000006s;\n      -o-transform: rotate(-45deg);\n      animation-delay: .44000000000000006s;\n      transform: rotate(-45deg)\n  }\n  #rotateG_03 {\n      left: 26px;\n      top: 3px;\n      -moz-animation-delay: .55s;\n      -moz-transform: rotate(0deg);\n      -webkit-animation-delay: .55s;\n      -webkit-transform: rotate(0deg);\n      -ms-animation-delay: .55s;\n      -ms-transform: rotate(0deg);\n      -o-animation-delay: .55s;\n      -o-transform: rotate(0deg);\n      animation-delay: .55s;\n      transform: rotate(0deg)\n  }\n  #rotateG_04 {\n      right: 8px;\n      top: 10px;\n      -moz-animation-delay: .66s;\n      -moz-transform: rotate(45deg);\n      -webkit-animation-delay: .66s;\n      -webkit-transform: rotate(45deg);\n      -ms-animation-delay: .66s;\n      -ms-transform: rotate(45deg);\n      -o-animation-delay: .66s;\n      -o-transform: rotate(45deg);\n      animation-delay: .66s;\n      transform: rotate(45deg)\n  }\n  #rotateG_05 {\n      right: 0;\n      top: 28px;\n      -moz-animation-delay: .7700000000000001s;\n      -moz-transform: rotate(90deg);\n      -webkit-animation-delay: .7700000000000001s;\n      -webkit-transform: rotate(90deg);\n      -ms-animation-delay: .7700000000000001s;\n      -ms-transform: rotate(90deg);\n      -o-animation-delay: .7700000000000001s;\n      -o-transform: rotate(90deg);\n      animation-delay: .7700000000000001s;\n      transform: rotate(90deg)\n  }\n  #rotateG_06 {\n      right: 8px;\n      bottom: 7px;\n      -moz-animation-delay: .8800000000000001s;\n      -moz-transform: rotate(135deg);\n      -webkit-animation-delay: .8800000000000001s;\n      -webkit-transform: rotate(135deg);\n      -ms-animation-delay: .8800000000000001s;\n      -ms-transform: rotate(135deg);\n      -o-animation-delay: .8800000000000001s;\n      -o-transform: rotate(135deg);\n      animation-delay: .8800000000000001s;\n      transform: rotate(135deg)\n  }\n  #rotateG_07 {\n      bottom: 0;\n      left: 26px;\n      -moz-animation-delay: .99s;\n      -moz-transform: rotate(180deg);\n      -webkit-animation-delay: .99s;\n      -webkit-transform: rotate(180deg);\n      -ms-animation-delay: .99s;\n      -ms-transform: rotate(180deg);\n      -o-animation-delay: .99s;\n      -o-transform: rotate(180deg);\n      animation-delay: .99s;\n      transform: rotate(180deg)\n  }\n  #rotateG_08 {\n      left: 8px;\n      bottom: 7px;\n      -moz-animation-delay: 1.1s;\n      -moz-transform: rotate(-135deg);\n      -webkit-animation-delay: 1.1s;\n      -webkit-transform: rotate(-135deg);\n      -ms-animation-delay: 1.1s;\n      -ms-transform: rotate(-135deg);\n      -o-animation-delay: 1.1s;\n      -o-transform: rotate(-135deg);\n      animation-delay: 1.1s;\n      transform: rotate(-135deg)\n  }\n  \n  @keyframes fadeG {\n      0% {\n          background-color: #000\n      }\n  \n      100% {\n          background-color: #FFF\n      }\n  }\n\n  .label-warning {\n      background-color: red;\n  }\n\n  .bg-info {\n      background-color: white ;\n      background: rgba(255, 255, 255, 0.3) !important;\n  }\n\n  img.leftArrow {\n    width: 5rem; \n    height: 5rem;\n    position: absolute;\n    float: left;\n    margin-top: 15%;\n    margin-left: 15%;  \n   }\n\n   img.leftArrow:hover {\n    transform:scale(1.25);\n   }\n\n   img.leftArrow:active {\n    transform:scale(1.0);\n   }\n\n   img.rightArrow {\n    width: 5rem; \n    height: 5rem;\n    position: absolute;\n    float: right;\n    margin-top: 15%;\n    margin-left: 55%;  \n   }\n\n   img.rightArrow:hover {\n    transform:scale(1.25);\n   }\n\n   img.rightArrow:active {\n    transform:scale(1.0);\n   }\n\n   button.submit {\n    background-color: #4CAF50; /* Green */\n    border: none;\n    color: white;\n    padding: 15px 32px;\n    text-align: center;\n    text-decoration: none;\n    display: inline-block;\n    font-size: 16px;\n    margin-top: 32%;\n    margin-left: -30%;\n    border-radius: 20px;\n  }\n\n  button.submit:hover {\n    transform:scale(1.25);\n   }\n\n   button.submit:active {\n    transform:scale(1.0);\n   }\n\n.container-fluid {\n    margin-top: 1%;\n    position: fixed;\n    width: 70%;\n    height: 90%;\n   /* overflow-y: scroll;*/\n}   \n\nimg {\n    width: 55%;\n    margin-right: 3%;\n    margin-top: 10px;\n    margin-bottom: 10px;\n    margin-left: 10px;\n}\n\nh3.result {\n    text-align: justify;\n    position: absolute;\n    margin-left: 100px;\n}\n\n.box {\n  /*  background-color: #FFF;*/\n    height: 60%;\n    margin-top: 5%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/fill-in-questions/fill-in-questions.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  fill-in-questions works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/fill-in-questions/fill-in-questions.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FillInQuestionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FillInQuestionsComponent = (function () {
    function FillInQuestionsComponent() {
    }
    FillInQuestionsComponent.prototype.ngOnInit = function () {
    };
    FillInQuestionsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-fill-in-questions',
            template: __webpack_require__("../../../../../src/app/components/fill-in-questions/fill-in-questions.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/fill-in-questions/fill-in-questions.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FillInQuestionsComponent);
    return FillInQuestionsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/questions/questions.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#qid {\n    padding: 10px 15px;\n    border-radius: 20px;\n  }\n  label.btn {\n      padding: 18px 60px;\n      white-space: normal;\n      -webkit-transform: scale(1.0);\n      -moz-transform: scale(1.0);\n      -o-transform: scale(1.0);\n      -webkit-transition-duration: .3s;\n      -moz-transition-duration: .3s;\n      -o-transition-duration: .3s\n  }\n  \n  label.btn:hover {\n      text-shadow: 0 3px 2px rgba(0,0,0,0.4);\n      -webkit-transform: scale(1.1);\n      -moz-transform: scale(1.1);\n      -o-transform: scale(1.1)\n  }\n  label.btn-block {\n      text-align: left;\n      position: relative;\n      margin-top: 15px;\n  }\n  \n  label .btn-label {\n      position: absolute;\n      left: 0;\n      top: 0;\n      display: inline-block;\n      padding: 0 10px;\n      background: rgba(0,0,0,.15);\n      height: 100%\n  }\n  \n  label .glyphicon {\n      top: 34%\n  }\n  .element-animation1 {\n      animation: animationFrames ease .8s;\n      animation-iteration-count: 1;\n      transform-origin: 50% 50%;\n      -webkit-animation: animationFrames ease .8s;\n      -webkit-animation-iteration-count: 1;\n      -webkit-transform-origin: 50% 50%;\n      -ms-animation: animationFrames ease .8s;\n      -ms-animation-iteration-count: 1;\n      -ms-transform-origin: 50% 50%\n  }\n  .element-animation2 {\n      animation: animationFrames ease 1s;\n      animation-iteration-count: 1;\n      transform-origin: 50% 50%;\n      -webkit-animation: animationFrames ease 1s;\n      -webkit-animation-iteration-count: 1;\n      -webkit-transform-origin: 50% 50%;\n      -ms-animation: animationFrames ease 1s;\n      -ms-animation-iteration-count: 1;\n      -ms-transform-origin: 50% 50%\n  }\n  .element-animation3 {\n      animation: animationFrames ease 1.2s;\n      animation-iteration-count: 1;\n      transform-origin: 50% 50%;\n      -webkit-animation: animationFrames ease 1.2s;\n      -webkit-animation-iteration-count: 1;\n      -webkit-transform-origin: 50% 50%;\n      -ms-animation: animationFrames ease 1.2s;\n      -ms-animation-iteration-count: 1;\n      -ms-transform-origin: 50% 50%\n  }\n  .element-animation4 {\n      animation: animationFrames ease 1.4s;\n      animation-iteration-count: 1;\n      transform-origin: 50% 50%;\n      -webkit-animation: animationFrames ease 1.4s;\n      -webkit-animation-iteration-count: 1;\n      -webkit-transform-origin: 50% 50%;\n      -ms-animation: animationFrames ease 1.4s;\n      -ms-animation-iteration-count: 1;\n      -ms-transform-origin: 50% 50%\n  }\n  @keyframes animationFrames {\n      0% {\n          opacity: 0;\n          transform: translate(-1500px,0px)\n      }\n  \n      60% {\n          opacity: 1;\n          transform: translate(30px,0px)\n      }\n  \n      80% {\n          transform: translate(-10px,0px)\n      }\n  \n      100% {\n          opacity: 1;\n          transform: translate(0px,0px)\n      }\n  }\n\n  .modal-dialog {\n      margin-top: 5%;\n  }\n  \n  .modal-header {\n      background-color: transparent;\n      color: inherit\n  }\n  \n  .modal-body {\n      min-height: 355px\n  }\n  #loadbar {\n      position: absolute;\n      width: 62px;\n      height: 77px;\n      top: 2em\n  }\n  .blockG {\n      position: absolute;\n      background-color: #FFF;\n      width: 10px;\n      height: 24px;\n      -moz-border-radius: 8px 8px 0 0;\n      -moz-transform: scale(0.4);\n      -moz-animation-name: fadeG;\n      -moz-animation-duration: .8800000000000001s;\n      -moz-animation-iteration-count: infinite;\n      -moz-animation-direction: linear;\n      -webkit-border-radius: 8px 8px 0 0;\n      -webkit-transform: scale(0.4);\n      -webkit-animation-name: fadeG;\n      -webkit-animation-duration: .8800000000000001s;\n      -webkit-animation-iteration-count: infinite;\n      -webkit-animation-direction: linear;\n      -ms-border-radius: 8px 8px 0 0;\n      -ms-transform: scale(0.4);\n      -ms-animation-name: fadeG;\n      -ms-animation-duration: .8800000000000001s;\n      -ms-animation-iteration-count: infinite;\n      -ms-animation-direction: linear;\n      -o-border-radius: 8px 8px 0 0;\n      -o-transform: scale(0.4);\n      -o-animation-name: fadeG;\n      -o-animation-duration: .8800000000000001s;\n      -o-animation-iteration-count: infinite;\n      -o-animation-direction: linear;\n      border-radius: 8px 8px 0 0;\n      transform: scale(0.4);\n      animation-name: fadeG;\n      animation-duration: .8800000000000001s;\n      animation-iteration-count: infinite;\n      animation-direction: linear\n  }\n  #rotateG_01 {\n      left: 0;\n      top: 28px;\n      -moz-animation-delay: .33s;\n      -moz-transform: rotate(-90deg);\n      -webkit-animation-delay: .33s;\n      -webkit-transform: rotate(-90deg);\n      -ms-animation-delay: .33s;\n      -ms-transform: rotate(-90deg);\n      -o-animation-delay: .33s;\n      -o-transform: rotate(-90deg);\n      animation-delay: .33s;\n      transform: rotate(-90deg)\n  }\n  #rotateG_02 {\n      left: 8px;\n      top: 10px;\n      -moz-animation-delay: .44000000000000006s;\n      -moz-transform: rotate(-45deg);\n      -webkit-animation-delay: .44000000000000006s;\n      -webkit-transform: rotate(-45deg);\n      -ms-animation-delay: .44000000000000006s;\n      -ms-transform: rotate(-45deg);\n      -o-animation-delay: .44000000000000006s;\n      -o-transform: rotate(-45deg);\n      animation-delay: .44000000000000006s;\n      transform: rotate(-45deg)\n  }\n  #rotateG_03 {\n      left: 26px;\n      top: 3px;\n      -moz-animation-delay: .55s;\n      -moz-transform: rotate(0deg);\n      -webkit-animation-delay: .55s;\n      -webkit-transform: rotate(0deg);\n      -ms-animation-delay: .55s;\n      -ms-transform: rotate(0deg);\n      -o-animation-delay: .55s;\n      -o-transform: rotate(0deg);\n      animation-delay: .55s;\n      transform: rotate(0deg)\n  }\n  #rotateG_04 {\n      right: 8px;\n      top: 10px;\n      -moz-animation-delay: .66s;\n      -moz-transform: rotate(45deg);\n      -webkit-animation-delay: .66s;\n      -webkit-transform: rotate(45deg);\n      -ms-animation-delay: .66s;\n      -ms-transform: rotate(45deg);\n      -o-animation-delay: .66s;\n      -o-transform: rotate(45deg);\n      animation-delay: .66s;\n      transform: rotate(45deg)\n  }\n  #rotateG_05 {\n      right: 0;\n      top: 28px;\n      -moz-animation-delay: .7700000000000001s;\n      -moz-transform: rotate(90deg);\n      -webkit-animation-delay: .7700000000000001s;\n      -webkit-transform: rotate(90deg);\n      -ms-animation-delay: .7700000000000001s;\n      -ms-transform: rotate(90deg);\n      -o-animation-delay: .7700000000000001s;\n      -o-transform: rotate(90deg);\n      animation-delay: .7700000000000001s;\n      transform: rotate(90deg)\n  }\n  #rotateG_06 {\n      right: 8px;\n      bottom: 7px;\n      -moz-animation-delay: .8800000000000001s;\n      -moz-transform: rotate(135deg);\n      -webkit-animation-delay: .8800000000000001s;\n      -webkit-transform: rotate(135deg);\n      -ms-animation-delay: .8800000000000001s;\n      -ms-transform: rotate(135deg);\n      -o-animation-delay: .8800000000000001s;\n      -o-transform: rotate(135deg);\n      animation-delay: .8800000000000001s;\n      transform: rotate(135deg)\n  }\n  #rotateG_07 {\n      bottom: 0;\n      left: 26px;\n      -moz-animation-delay: .99s;\n      -moz-transform: rotate(180deg);\n      -webkit-animation-delay: .99s;\n      -webkit-transform: rotate(180deg);\n      -ms-animation-delay: .99s;\n      -ms-transform: rotate(180deg);\n      -o-animation-delay: .99s;\n      -o-transform: rotate(180deg);\n      animation-delay: .99s;\n      transform: rotate(180deg)\n  }\n  #rotateG_08 {\n      left: 8px;\n      bottom: 7px;\n      -moz-animation-delay: 1.1s;\n      -moz-transform: rotate(-135deg);\n      -webkit-animation-delay: 1.1s;\n      -webkit-transform: rotate(-135deg);\n      -ms-animation-delay: 1.1s;\n      -ms-transform: rotate(-135deg);\n      -o-animation-delay: 1.1s;\n      -o-transform: rotate(-135deg);\n      animation-delay: 1.1s;\n      transform: rotate(-135deg)\n  }\n  \n  @keyframes fadeG {\n      0% {\n          background-color: #000\n      }\n  \n      100% {\n          background-color: #FFF\n      }\n  }\n\n  .label-warning {\n      background-color: red;\n  }\n\n  .bg-info {\n      background-color: white ;\n      background: rgba(255, 255, 255, 0.3) !important;\n  }\n\n  .leftArrow {\n    width: 5rem; \n    height: 5rem;\n    position: absolute;\n    float: left;\n   }\n\n   img.leftArrow:hover {\n    transform:scale(1.25);\n   }\n\n   img.leftArrow:active {\n    transform:scale(1.0);\n   }\n\n   img.rightArrow {\n    width: 5rem; \n    height: 5rem;\n    position: absolute;\n    float: right;\n    margin-top: 15%;\n    margin-left: 55%;  \n   }\n\n   img.rightArrow:hover {\n    transform:scale(1.25);\n   }\n\n   img.rightArrow:active {\n    transform:scale(1.0);\n   }\n\n   button.submit {\n    background-color: #4CAF50; /* Green */\n    border: none;\n    color: white;\n    padding: 15px 32px;\n    text-align: center;\n    text-decoration: none;\n    display: inline-block;\n    font-size: 16px;\n    margin-top: 32%;\n    margin-left: -30%;\n    border-radius: 20px;\n  }\n\n  button.submit:hover {\n    transform:scale(1.25);\n   }\n\n   button.submit:active {\n    transform:scale(1.0);\n   }\n\n.container-fluid {\n    margin-top: 1%;\n    position: fixed;\n    width: 70%;\n    height: 90%;\n   /* overflow-y: scroll;*/\n}\n\nh3.result {\n    background: rgba(0, 0, 0, 0.5) !important;\n}\n\n.box {\n  /*  background-color: #FFF;*/\n    height: 60%;\n    margin-top: 5%;\n}\n\ninput.center {\n    margin-top: 25%;\n}\n\nbutton.next {\n    background-color: green;\n    float: right;\n    margin-top: 115px;\n}\n\n.carousel {\n    width: 110%;\n    height: 500px;\n  }\n\n  .carousel-inner img {\n    margin: auto;\n  }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/questions/questions.component.html":
/***/ (function(module, exports) {

module.exports = "<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\n<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js\"></script>\n<img class=\"leftArrow\" src=\"\\assets\\left_arrow.png\" alt=\"Back\" align=\"left\" (click)=\"goToHomePage()\">\n\n<div *ngIf=\"!showResult\" class=\"container-fluid bg-info\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h3 *ngIf=\"start\">\n                    <span class=\"label label-warning\" id=\"qid\">{{nr}}</span> {{currentQuestion.question}}</h3>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"col-xs-3 col-xs-offset-5\">\n                    <div *ngIf=\"waitingForAnswer\" id=\"loadbar\">\n                        <div class=\"blockG\" id=\"rotateG_01\"></div>\n                        <div class=\"blockG\" id=\"rotateG_02\"></div>\n                        <div class=\"blockG\" id=\"rotateG_03\"></div>\n                        <div class=\"blockG\" id=\"rotateG_04\"></div>\n                        <div class=\"blockG\" id=\"rotateG_05\"></div>\n                        <div class=\"blockG\" id=\"rotateG_06\"></div>\n                        <div class=\"blockG\" id=\"rotateG_07\"></div>\n                        <div class=\"blockG\" id=\"rotateG_08\"></div>\n                    </div>\n                </div>\n\n                <div *ngIf=\"start && quizType != 'FILL_IN'\">\n                    <div class=\"quiz\" id=\"quiz\" data-toggle=\"buttons\" *ngFor=\"let answer of currentQuestion.answers, let i = index\">\n                        <label (click)=\"getNextQuestion(answer)\" class=\"element-animation1 btn btn-lg btn-primary btn-block\">\n                            <span class=\"btn-label\">\n                                <i class=\"glyphicon glyphicon-chevron-right\"></i>\n                            </span>\n                            <input type=\"radio\" name=\"q_answer\" value=\"answer.answerPoint\">{{answer.answer}}</label>\n                    </div>\n\n                </div>\n\n                <div *ngIf=\"start && quizType == 'FILL_IN'\">\n                    <!--  <div class=\"quiz\" id=\"quiz\" data-toggle=\"buttons\" *ngFor=\"let answer of currentQuestion.answers, let i = index\">\n         <label (click)=\"getNextQuestion(answer)\" class=\"element-animation1 btn btn-lg btn-primary btn-block\"><span class=\"btn-label\"><i class=\"glyphicon glyphicon-chevron-right\"></i></span> <input type=\"radio\" name=\"q_answer\" value=\"answer.answerPoint\">{{answer.answer}}</label>\n     </div> -->\n                    <div class=\"quiz\">\n                        <form [formGroup]=\"answerForm\" ngNativeValidate>\n                            <input formControlName=\"answer\" type=\"text\" class=\"form-control center\" id=\"{{nr}}\" placeholder=\"Answer\" #answer>\n                        </form>\n                        <button [disabled]=\"!answerForm.valid\" class=\"btn btn-primary next\" type=\"submit\" (click)=\"getNextFillInQUestion(answer.value)\">Next ></button>\n                    </div>\n                </div>\n            </div>\n            <div class=\"modal-footer text-muted\">\n                <span id=\"answer\"></span>\n            </div>\n        </div>\n    </div>\n</div> \n\n<div *ngIf=\"showResult\" class=\"container box\">\n    <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\"> \n        <div class=\"carousel-inner\">\n            <div class=\"carousel-item active\">\n                <img src=\"{{result.imagePath}}\" class=\"d-block\" alt=\"...\">\n                <div class=\"carousel-caption d-none d-md-block\">\n                    <h3 class=\"result\">{{result.result}}</h3>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/questions/questions.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_quizzes_quiz_service__ = __webpack_require__("../../../../../src/app/services/quizzes/quiz.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_questions_questions_service__ = __webpack_require__("../../../../../src/app/services/questions/questions.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_models__ = __webpack_require__("../../../../../src/app/models/models.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var QuestionsComponent = (function () {
    function QuestionsComponent(quizService, questionsService, router) {
        var _this = this;
        this.quizService = quizService;
        this.questionsService = questionsService;
        this.router = router;
        this.givenAnswers = [];
        this.quizService.currentQuizType.subscribe(function (quizType) { return _this.quizType = quizType; });
        this.quizService.currentQuizId.subscribe(function (quizId) { return _this.quizId = quizId; });
        this.start = false;
        this.showResult = false;
        this.waitingForAnswer = false;
        this.answerForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({
            answer: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["e" /* Validators */].minLength(1)]),
        });
    }
    QuestionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.waitingForAnswer = true;
        setTimeout(function () {
            return _this.questionsService.getTheQuizQuestions(_this.quizType, _this.quizId).subscribe(function (resp) {
                _this.questions = resp;
                _this.waitingForAnswer = false;
                _this.currentQuestion = _this.questions[0];
                _this.nr = 1;
                _this.start = true;
            }, function (error) {
                // this.notifications.showErrorMessage("cantLoadBssket");
            });
        }, 100);
    };
    QuestionsComponent.prototype.initForm = function () {
        this.answerForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({
            answer: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["e" /* Validators */].minLength(1)]),
        });
    };
    QuestionsComponent.prototype.getNextQuestion = function (answer) {
        this.givenAnswers.push(answer);
        if (this.questions.length > this.nr) {
            this.currentQuestion = this.questions[this.nr];
            this.nr += 1;
        }
        else {
            this.getResult();
        }
    };
    QuestionsComponent.prototype.getResult = function () {
        var _this = this;
        this.start = false;
        this.waitingForAnswer = true;
        setTimeout(function () {
            return _this.questionsService.getResult(_this.givenAnswers, _this.quizId, _this.quizType).subscribe(function (resp) {
                _this.result = resp;
                _this.waitingForAnswer = false;
                _this.showResult = true;
            }, function (error) {
                // this.notifications.showErrorMessage("cantLoadBssket");
            });
        }, 2000);
    };
    QuestionsComponent.prototype.getNextFillInQUestion = function (answerText) {
        var givenAnswer = new __WEBPACK_IMPORTED_MODULE_3__models_models__["a" /* Answer */](this.currentQuestion.questionId, answerText);
        this.givenAnswers.push(givenAnswer);
        if (this.questions.length > this.nr) {
            this.currentQuestion = this.questions[this.nr];
            this.nr += 1;
            this.answerForm.reset();
        }
        else {
            this.getResult();
        }
    };
    QuestionsComponent.prototype.goToHomePage = function () {
        this.router.navigate(['/']);
    };
    QuestionsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-questions',
            template: __webpack_require__("../../../../../src/app/components/questions/questions.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/questions/questions.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_quizzes_quiz_service__["a" /* QuizService */], __WEBPACK_IMPORTED_MODULE_2__services_questions_questions_service__["a" /* QuestionsService */], __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */]])
    ], QuestionsComponent);
    return QuestionsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/quizzes/quizzes.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container{\n    width: 70%;\n}\n\n.card{\n    border: none;\n    background: rgba(255, 255, 255, 0);\n    margin-left: 45px;\n}\n\n.row {\n    background: rgba(255, 255, 255, 0.5);\n    padding-bottom: 15px;\n}\n\n.text-muted {\n    color: #636c72 !important;\n}\n\n.red{\n    color: red;\n}\n\n.black{\n    color: black;\n}\n\nbutton.takeQuiz{\n    background-color: red;\n    color: white;\n    text-align: center;\n    text-decoration: none;\n    border: none;\n    padding-top: 7px;\n    padding-bottom: 7px;\n    width: 91%;\n    margin-left: 1rem;\n}\n\nbutton.takeQuiz:hover{\n    background-color:darkred;\n}\n\nspan.text-content {\n  background: rgba(0,0,0,0.5);\n  color: white;\n  cursor: pointer;\n  display: table;\n  /*height: 280px;*/\n /* left: 0;*/\n  position: absolute;\n  top: 0;\n  /*width: auto;*/\n  opacity: 0;\n  width: 92%;\n  max-width: 92%;\n  height: 100%;\n}\n\n\nspan.text-content span {\n  display: table-cell;\n  text-align: center;\n  vertical-align: middle;\n}\n\na.img:hover span.text-content {\n  opacity: 1;\n}\n\na.color{\n  color: black;\n  text-decoration: none;\n}\n\ndiv.container-fluid{\n    position: relative;\n    width: 100%;\n    height: 100%;\n    overflow: hidden;\n    margin: 10px 0;\n    z-index: 1;\n}\n\n\ndiv.center{\n    margin-left: 30%;\n}\n\np.center{\n    text-align: center;\n    -ms-flex-line-pack: center;\n        align-content: center;\n}\n\np{\n    margin-left: 1rem;\n}\n\n.left{\n    float: right;\n}\n\n.red{\n    background-color: red;\n    border-color: red;\n    color: white;\n}\n\n.red:hover{\n    background-color: darkred;\n    border-color: darkred;\n    color: white;\n}\n\n.orange{\n    background-color: orangered;\n    border-color: orangered;\n}\n\n.orange:hover{\n    background-color: rgba(255, 68, 0, 0.747);\n    border-color: rgba(255, 68, 0, 0.747);\n}\n\n.yellow{\n    background-color: blue;\n    border-color: blue;\n}\n\n.yellow:hover{\n    background-color: darkblue;\n    border-color: darkblue;\n}\n\n.green{\n    background-color: darkgreen;\n    border-color: darkgreen;\n}\n\n.green:hover{\n    background-color: rgb(1, 83, 1);\n    border-color: rgb(1, 83, 1);\n}\n\n.btn-secondary {\n    margin-left: 30px !important;\n    margin-top: 12px;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/quizzes/quizzes.component.html":
/***/ (function(module, exports) {

module.exports = "<button type=\"button\" class=\"btn btn-secondary btn-lg left green\" (click)=\"getQuizzesByTheme('FILL_IN')\">A-Z</button>\n<button type=\"button\" class=\"btn btn-secondary btn-lg left yellow\" (click)=\"getQuizzesByTheme('PICK_ONE_CORRECT')\">Knowledge</button>\n<button type=\"button\" class=\"btn btn-secondary btn-lg left orange\" (click)=\"getQuizzesByTheme('PICK_ONE')\">Personality</button>\n<button type=\"button\" class=\"btn btn-secondary btn-lg left red\" (click)=\"getQuizzesByTheme('all')\">All Quizzes</button>\n<br />\n<br />\n<br />\n\n<div class=\"row\">\n  <div class=\"card\" style=\"width: 20rem;\" *ngFor=\"let quiz of quizzes, let i = index\">\n    <div class=\"container-fluid\">\n      <a class=\"img card-img-top\"><img style=\"width: 20rem; height: 15rem;\" data-src=\"holder.js/100px280/thumb\" class=\"card-img-top img-fluid\"\n          src=\"{{quiz.imagePath}}\" data-holder-rendered=\"true\" />\n        <span class=\"text-content\"><span>{{quiz.description}}</span></span>\n      </a>\n    </div>\n    <button class=\"takeQuiz\" type=\"button\" (click)=\"takeQuiz(i)\"><b>Take {{quiz.name}}</b></button>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/quizzes/quizzes.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuizzesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_quizzes_quiz_service__ = __webpack_require__("../../../../../src/app/services/quizzes/quiz.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var QuizzesComponent = (function () {
    function QuizzesComponent(quizService, router) {
        this.quizService = quizService;
        this.router = router;
    }
    QuizzesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.quizService.getQuizzes().subscribe(function (resp) {
            _this.quizzes = resp;
        }, function (error) {
            // this.notifications.showErrorMessage("cantLoadBssket");
        });
    };
    QuizzesComponent.prototype.takeQuiz = function (i) {
        this.quizService.changeQuizId(this.quizzes[i].quizId);
        this.quizService.changeQuizType(this.quizzes[i].type);
        this.router.navigate(['/quiz/' + this.quizzes[i].name]);
    };
    QuizzesComponent.prototype.getQuizzesByTheme = function (type) {
        var _this = this;
        console.log(type);
        if (type == 'all') {
            this.quizService.getQuizzes().subscribe(function (resp) {
                _this.quizzes = resp;
            }, function (error) {
                // this.notifications.showErrorMessage("cantLoadBssket");
            });
        }
        else {
            this.quizService.getQuizzesByType(type).subscribe(function (resp) {
                _this.quizzes = resp;
            }, function (error) {
                // this.notifications.showErrorMessage("cantLoadBssket");
            });
        }
    };
    QuizzesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-quizzes',
            template: __webpack_require__("../../../../../src/app/components/quizzes/quizzes.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/quizzes/quizzes.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_quizzes_quiz_service__["a" /* QuizService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], QuizzesComponent);
    return QuizzesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/models/models.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Quiz */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Answer; });
/* unused harmony export Question */
/* unused harmony export Result */
var Quiz = (function () {
    function Quiz() {
    }
    return Quiz;
}());

var Answer = (function () {
    function Answer(questionId, answer) {
        this.questionId = questionId;
        this.answer = answer;
    }
    return Answer;
}());

var Question = (function () {
    function Question() {
    }
    return Question;
}());

var Result = (function () {
    function Result() {
    }
    return Result;
}());



/***/ }),

/***/ "../../../../../src/app/services/questions/questions.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var QuestionsService = (function () {
    function QuestionsService(http) {
        this.http = http;
    }
    QuestionsService.prototype.getTheQuizQuestions = function (quizType, quizId) {
        return this.http.get('/api/questions/' + quizId + '/' + quizType)
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].throw('Server error(getTheQuizQuestions)'); });
    };
    QuestionsService.prototype.getResult = function (answers, quizId, quizType) {
        return this.http.post('/api/result/evaluate/' + quizId + '/' + quizType, answers)
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].throw('Server error(getResult)'); });
    };
    QuestionsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], QuestionsService);
    return QuestionsService;
}());



/***/ }),

/***/ "../../../../../src/app/services/quizzes/quiz.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuizService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var QuizService = (function () {
    function QuizService(http) {
        this.http = http;
        this.quizTypeSource = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]("");
        this.currentQuizType = this.quizTypeSource.asObservable();
        this.quizId = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */](0);
        this.currentQuizId = this.quizId.asObservable();
    }
    QuizService.prototype.getQuizzes = function () {
        return this.http.get('/api/quiz/quizzes')
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].throw('Server error(getQuizzes)'); });
    };
    QuizService.prototype.getQuizzesByType = function (type) {
        return this.http.get('/api/quiz/quizzes/' + type)
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].throw('Server error(getQuizzesByType)'); });
    };
    QuizService.prototype.changeQuizType = function (type) {
        this.quizTypeSource.next(type);
    };
    QuizService.prototype.changeQuizId = function (quizId) {
        this.quizId.next(quizId);
    };
    QuizService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], QuizService);
    return QuizService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map