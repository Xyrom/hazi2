import { Component, OnInit } from '@angular/core';
import { Quiz } from '../../models/models';
import { QuizService } from '../../services/quizzes/quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quizzes',
  templateUrl: './quizzes.component.html',
  styleUrls: ['./quizzes.component.css']
})
export class QuizzesComponent implements OnInit {

  quizzes: Quiz[];

  constructor(private quizService: QuizService, private router: Router) { }

  ngOnInit() {
    this.quizService.getQuizzes().subscribe((resp) => {
      this.quizzes = resp;
    }, (error) => {
      // this.notifications.showErrorMessage("cantLoadBssket");
    });
  }

  takeQuiz(i) {
    this.quizService.changeQuizId(this.quizzes[i].quizId);
    this.quizService.changeQuizType(this.quizzes[i].type);
    this.router.navigate(['/quiz/' + this.quizzes[i].name]);
  }

  getQuizzesByTheme(type: String) {
    console.log(type);
    if (type == 'all') {
      this.quizService.getQuizzes().subscribe((resp) => {
        this.quizzes = resp;
      }, (error) => {
        // this.notifications.showErrorMessage("cantLoadBssket");
      });
    } else {
      this.quizService.getQuizzesByType(type).subscribe((resp) => {
        this.quizzes = resp;
      }, (error) => {
        // this.notifications.showErrorMessage("cantLoadBssket");
      });
    }
  }

}
