import { Component, OnInit } from '@angular/core';
import { QuizService } from '../../services/quizzes/quiz.service';
import { QuestionsService } from '../../services/questions/questions.service';
import { Question, Answer, Result } from '../../models/models';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})

export class QuestionsComponent implements OnInit {

  quizType: String;
  quizId: number;
  questions: Question[];
  currentQuestion: Question;
  start: boolean;
  nr: number;

  givenAnswers: Answer[] = [];
  result: Result;
  showResult: boolean;
  waitingForAnswer: boolean;

  answerForm: FormGroup;

  constructor(private quizService: QuizService, private questionsService: QuestionsService, private router:Router) {
    this.quizService.currentQuizType.subscribe(quizType => this.quizType = quizType);
    this.quizService.currentQuizId.subscribe(quizId => this.quizId = quizId);
    this.start = false;
    this.showResult = false;
    this.waitingForAnswer = false;
    this.answerForm = new FormGroup({
      answer: new FormControl('', [Validators.required, Validators.minLength(1)]),
    });
  }

  ngOnInit() {
    this.waitingForAnswer = true;
    setTimeout(() =>
      this.questionsService.getTheQuizQuestions(this.quizType, this.quizId).subscribe((resp) => {
        this.questions = resp;
        this.waitingForAnswer = false;
        this.currentQuestion = this.questions[0];
        this.nr = 1;
        this.start = true;
      }, (error) => {
        // this.notifications.showErrorMessage("cantLoadBssket");
      }), 100);

  }

  initForm() {
    this.answerForm = new FormGroup({
      answer: new FormControl('', [Validators.required, Validators.minLength(1)]),
    });
  }

  getNextQuestion(answer) {
    this.givenAnswers.push(answer);
    if (this.questions.length > this.nr) {
      this.currentQuestion = this.questions[this.nr];
      this.nr += 1;
    } else {
      this.getResult();
    }
  }

  getResult() {
    this.start = false;
    this.waitingForAnswer = true;
    setTimeout(() =>
      this.questionsService.getResult(this.givenAnswers, this.quizId, this.quizType).subscribe((resp) => {
        this.result = resp;
        this.waitingForAnswer = false;
        this.showResult = true;
      }, (error) => {
        // this.notifications.showErrorMessage("cantLoadBssket");
      }), 2000);
  }

  getNextFillInQUestion(answerText) {
    let givenAnswer: Answer = new Answer(this.currentQuestion.questionId, answerText);
    this.givenAnswers.push(givenAnswer);

    if (this.questions.length > this.nr) {
      this.currentQuestion = this.questions[this.nr];
      this.nr += 1;
      this.answerForm.reset();
    } else {
      this.getResult();
    }

  }

  goToHomePage() {
    this.router.navigate(['/']);
  }

}
