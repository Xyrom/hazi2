export class Quiz {
    quizId: number;
    name: string;
    description: string;
    type: string;
    imagePath: string
  }

  export class Answer {
    answerId: number;
    answer: string;
    answersPoint: number;
    questionId: number;
    
    constructor(questionId: number, answer: string) {
      this.questionId = questionId;
      this.answer = answer;
    }

  }

  export class Question {
    questionId: number;
    question: string;
    answers: Answer[];
  }

  export class Result {
    resultnId: number;
    result: string;
    imagePath: string;
  }