import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { QuestionsComponent } from './components/questions/questions.component';
import { QuizzesComponent } from './components/quizzes/quizzes.component';

import { QuizService } from './services/quizzes/quiz.service';
import { QuestionsService } from './services/questions/questions.service';
import { FillInQuestionsComponent } from './components/fill-in-questions/fill-in-questions.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes = [
    { path: 'quiz/:name', component: QuestionsComponent },
    { path: '', component: QuizzesComponent, pathMatch: 'full' }
]

@NgModule({
  declarations: [
    AppComponent,
    QuizzesComponent,
    QuestionsComponent,
    FillInQuestionsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    
  ],
  providers: [QuestionsService, QuizService],
  bootstrap: [AppComponent]
})
export class AppModule { }
