import { Injectable } from '@angular/core';
import { Response, RequestOptionsArgs, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Answer } from '../../models/models';

@Injectable()
export class QuestionsService {

  constructor(private http:HttpClient) { }

  getTheQuizQuestions(quizType:String, quizId:number) {
    return this.http.get('/api/questions/' + quizId + '/' + quizType)
    .catch((error: any) => Observable.throw('Server error(getTheQuizQuestions)'));
  }

  getResult(answers:Answer[], quizId, quizType) {
    return this.http.post('/api/result/evaluate/' + quizId + '/' + quizType, answers)
    .catch((error: any) => Observable.throw('Server error(getResult)'));
  }

}
