import { Injectable } from '@angular/core';
import { Response, RequestOptionsArgs, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class QuizService {

  private quizTypeSource = new BehaviorSubject<String>("");
  currentQuizType = this.quizTypeSource.asObservable();

  private quizId = new BehaviorSubject<number>(0);
  currentQuizId = this.quizId.asObservable();

  constructor(private http: HttpClient) { }

  getQuizzes() {
    return this.http.get('/api/quiz/quizzes')
      .catch((error: any) => Observable.throw('Server error(getQuizzes)'));
  }

  getQuizzesByType(type: String) {
    return this.http.get('/api/quiz/quizzes/' + type)
      .catch((error: any) => Observable.throw('Server error(getQuizzesByType)'));
  }

  changeQuizType(type: String) {
    this.quizTypeSource.next(type);
  }

  changeQuizId(quizId: number) {
    this.quizId.next(quizId);
  }

}
